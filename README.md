# Headrest

![Headrest](photos/version-1/01--first-wooden-brackets/2022-10-27%2010.08.16.jpg)

## Overview

This headrest design provides head support for a Go Baby Go user. It achieves this by extending 
up the bracing behind the user above the rear trunk.

Design details:

-   The bracing and support are fabricated from wood, PVC, and 3D printed brackets.

-   The headrest itself is fabricated from pool noodles and kickboards.

-   The design is attached to the car using toggle bolts. This allows the brackets to be installed 
    in long and narrow areas that cannot easily accommodate the width of a nut.

-   The top brackets allow the angle of the headrest to adjust to fit the user's needs.

-   The headrest is removable from the bracing to allow for easier storage and transport.


## How to Use this Project

Please treat this design as a baseline / stepping stone for your own headrest design. If you have 
the same model car as shown in the photos, you can reproduce the design demonstrated here to create 
a custom headrest for it. If you have a different car, the materials included should hopefully 
guide you in adding headrest support to your own cars.

We encourage all to customize their headrests (both functionally and visually) as needed for the 
target user.


## Materials Included

The following materials are included in this repository:

-   [Design](design)
    -   [CAD Files](design/cad-files)
        -   Fusion 360 design files for the headrest.
    -   [STL Files](design/stl-files)
        -   STL files for parts that should be 3D printed.

-   [Docs](docs)
    -   Supplemental design and fabrication documents.

-   [Photos](photos)
    -   [Version 1](photos/version-1)
        -   Photos detailing the fabrication of version 1 of this headrest.


## Version Information

This is version 1.0 of this headrest. Please see the [Changelog](Changelog.md) for a detailed 
version history.


## License

This project and materials are released under the [Unlicense](LICENSE). We provide the materials 
included here as a reference to anyone working in the Go Baby Go space. We hope this serves as a 
stepping stone to improved designs and would love to hear back on any improvements made on what we 
have created here.


