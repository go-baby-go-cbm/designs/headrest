# Component List
----------------------------------------------------------------------------------------------------
This is a summary of all modeled components. This does not include items such as mechanical 
fasteners, wires, etc.

   1. Bottom Bracket
      -  Usage:              Secure PCV pipe used to support the headrest to the top of the trunk.
      -  Fabrication:        3D print
      -  Quantity Needed:    2
   
   2. Top Bracket
      -  Usage:              Secure PVC pipe used to attach the removable headres to the bottom 
                            bumper of the car.
      -  Fabrication:        cut from wood (i.e. 2x4)
      -  Quantity Needed:    1
   

