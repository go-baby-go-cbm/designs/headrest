# Changelog

*Versioning incremented automatically by Fusion 360 each time the project is saved. Full design 
history captured below*


----------------------------------------------------------------------------------------------------
## v3 [2022-10-30 17:56:50]

### Added
-   Fillets on sharp edges of top of part (5.00 mm in size).
-   Channels for hot glue on bottom of part (2.75 mm wide, 1.00 mm deep).

### Changed
-   Increased width of slot from 5.00 => 5.25 mm.


----------------------------------------------------------------------------------------------------
## v2 [2022-10-30 17:06:56]

### Added
-   Support for parameters for key dimensions.
-   A 20 mm top brace for PVC pipe (with a 10 mm fillet to smooth the transition).

### Changed
-   Switched from inches to millimeters for document dimensions.
-   Increased height from 1.0" (25.4 mm) to 35.0 mm.
-   Increased padding around slot from 0.25" (6.35 mm) to 8.00 mm.
-   Increased padding around PVC hole from 0.25" (6.35 mm) to 10.0 mm.
-   Increased overall length from 5.50" (139.70 mm) to 160 mm (~5.9")
-   Increased slot length from 1.0" to 40 mm (1.57").
-   Increased PVC hole from 7/8" (22.225 mm) to 25.4 mm (1.0").


----------------------------------------------------------------------------------------------------
## v1 [2022-10-30 15:59:25]

### Added
-   Created CAD project and component.
-   Modeled up bracket based on first bracket produced. That is, all dimensions and features are 
    taken via reference measurements from a produced product and are slightly off the target design 
    (e.g. a 6-degree cutout instead of 10 on the bottom).


